@extends('layouts.public')

@section('content')
<div class="row">
    <div class="col-md-12 text-center">
        <h1>Welcome to Peoples!</h1>

        @if(Session::has('activation_code'))

        <p><a href="{{  URL::to('activate', array('id' => Session::get('userId'), urlencode(Session::get('activation_code')))) }}">Click this link to activate your account.</a></p>
        @endif
    </div>
</div>





@stop