<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="">
    <meta name="author" content="Don Wildman">
    <link rel="icon" href="{{ public_path('favicon.ico') }}">

    <title>People Manager</title>
	  
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('jquery-ui/jquery-ui.theme.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-switch.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">
	  
    <link href="{{ asset('fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/defaults.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

	  <script src="{{ asset('js/jquery.min.js') }}"></script>
  </head>

  <body>
  <nav class="navbar navbar-inverse navbar-fixed-top">
	  <div class="container-fluid">
		  <div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				  <span class="sr-only">Toggle navigation</span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
				  <span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="/en">Peoples</a>
		  </div>
		  <div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav navbar-right">
				  @if(Sentry::check())
				  <li><a href="/logout"><i class="fa fa-sign-out"></i> {{ trans('messages.logout') }}</a></li>
				  @endif
				  @if(Request::path() == 'register')
					  <li>
						  <div class="input-group m-t-10">
							  <div class="input-group-btn">
								  <button type="button" class="btn btn-default dropdown-toggle input-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ trans('messages.changeLabel') }} <span class="caret"></span></button>
								  {{ LanguageManager::generateDropList(Session::get('current_language')) }}

							  </div><!-- /btn-group -->
							  <input type="text" class="form-control input-sm" value="{{ LanguageManager::getHumanLanguage() }}">
						  </div><!-- /input-group -->
					  </li>
				  @endif
			  </ul>
		  </div>
	  </div>
  </nav>

    <div class="container">
		<div class="row m-t-30">
			<div class="col-md-12">
				@include('notifications')
				@yield('content')
			</div>
		</div>

    </div>

	<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('jquery-ui/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap-switch.min.js') }}"></script>
	<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ asset('js/ie10-viewport-bug-workaround.js') }}"></script>
	  
	  <script>
		  function changeLanguage()
		  {
			  var _lang = $('#language_dd').val();
			  window.location.assign('/change-language/' + _lang);
		  }
	  </script>
  </body>
</html>