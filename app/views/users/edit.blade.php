@extends('layouts.default')



{{-- Content --}}
@section('content')
<div class="row">
	<div class="col-md-12">

		<h2><i class="fa fa-edit"></i> Edit
			@if ($user->email == Sentry::getUser()->email)
			Your
			@else
			{{ $user->first_name }}'s
			@endif
			Profile</h2>

		<fieldset>
			<legend>Personal Details</legend>
			{{ Form::model($user, ['url' => '/edit/'.$user->id, 'role'=>'form', 'class' => 'form-horizontal']) }}

			<div class="form-group {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
				<label class="col-md-3 control-label" for="first_name">First Name:</label>
				<div class="col-md-4">
					{{ Form::text('first_name', null, ['class'=>'form-control', 'id'=>'first_name', 'placeholder'=>'First Name']) }}
					<span class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</span>
				</div>
			</div>

			<div class="form-group {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
				<label class="col-md-3 control-label" for="last_name">Last Name:</label>
				<div class="col-md-4">
					{{ Form::text('last_name', null, ['class'=>'form-control', 'id'=>'last_name', 'placeholder'=>'Last Name']) }}
					<span class="help-block">{{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}</span>
				</div>
			</div>

			<div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
				<label class="col-md-3 control-label" for="email">E-mail:</label>
				<div class="col-md-4">
					{{ Form::email('email', null, ['class'=>'form-control', 'id'=>'email', 'placeholder'=>'E-mail']) }}
					<span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
				</div>
			</div>

			<div class="form-group {{ ($errors->has('mobile')) ? 'has-error' : '' }}">
				<label class="col-md-3 control-label" for="mobile">Mobile No.:</label>
				<div class="col-md-4">
					{{ Form::text('mobile', null, ['class'=>'form-control mobile', 'id'=>'mobile', 'placeholder'=>'Mobile Number']) }}
					<span class="help-block">{{ ($errors->has('mobile') ? $errors->first('mobile') : '') }}</span>
				</div>
				<div class="col-md-5">
					<p class="form-control-static text-muted">Format: 073-123-4567</p>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label" for="language">Language:</label>
				<div class="col-md-4">
					<select name="language" class="form-control">
						{{ LanguageManager::generateDropDown($user->language) }}
					</select>
				</div>
			</div>


			<div class="form-group {{ ($errors->has('birthday')) ? 'has-error' : '' }}">
				<label class="col-md-3 control-label" for="birthday">Date of Birth:</label>
				<div class="col-md-4">
					{{ Form::text('birthday', null, ['class'=>'form-control datepicker', 'id'=>'birthday', 'placeholder'=>'Date of Birth']) }}
					<span class="help-block">{{ ($errors->has('birthday') ? $errors->first('birthday') : '') }}</span>
				</div>
			</div>

			<div class="col-md-4 col-md-offset-3">
				<button class="btn-primary btn" type="submit" >Update Profile</button>
			</div>

			{{ Form::close() }}
		</fieldset>


	</div>
</div>


<div class="row m-t-30">
	<div class="col-md-12">


		<fieldset>
			<legend><i class="fa fa-edit"></i> Change Password</legend>

		{{ Form::open(['url' => '/users/changepassword/'.$user->id, 'role'=>'form', 'class' => 'form-horizontal']) }}

		<div class="form-group {{ $errors->has('oldPassword') ? 'has error' : '' }}" for="oldPassword">
			<label class="col-md-3 control-label" for="oldPassword">Old Password</label>
			<div class="col-md-4">
				<input name="oldPassword" value="" type="password" class="form-control" placeholder="Old Password">
				{{ ($errors->has('oldPassword') ? $errors->first('oldPassword') : '') }}
			</div>
		</div>

		<div class="form-group {{ $errors->has('newPassword') ? 'has error' : '' }}" for="newPassword">
			<label class="col-md-3 control-label" for="newPassword">New Password</label>
			<div class="col-md-4">
				<input name="newPassword" value="" type="password" class="form-control" placeholder="New Password">
				{{ ($errors->has('newPassword') ?  $errors->first('newPassword') : '') }}
			</div>
		</div>

		<div class="form-group {{ $errors->has('newPassword_confirmation') ? 'has error' : '' }}" for="newPassword_confirmation">
			<label class="col-md-3 control-label" for="newPassword_confirmation">Confirm New Password</label>
			<div class="col-md-4">
				<input name="newPassword_confirmation" value="" type="password" class="form-control" placeholder="New Password Again">
				{{ ($errors->has('newPassword_confirmation') ? $errors->first('newPassword_confirmation') : '') }}
			</div>
		</div>


		<div class="col-md-offset-3 col-md-4">
			<button class="btn-primary btn" type="submit" >Change Password</button>
		</div>


		{{ Form::close() }}
		</fieldset>
	</div>
</div>

<div class="row m-t-30">
	<div class="col-md-12">

		<fieldset>
			<legend><i class="fa fa-group"></i> Group Memberships</legend>


		<form class="form-horizontal" action="/users/updatememberships/{{ $user->id }}" method="post" role="form">
				{{ Form::token() }}

				<table class="table">
					<thead>
						<th>Group</th>
						<th>Membership Status</th>
					</thead>
					<tbody>
						@foreach ($allGroups as $group)
							<tr>
								<td>{{ $group->name }}</td>
								<td>

										<input class="to-switch input-sm" name="permissions[{{ $group->id }}]" type="checkbox" {{ ( $user->inGroup($group)) ? 'checked' : '' }} >

								</td>
							</tr>
						@endforeach
					</tbody>
				</table>

					<div class="col-md-offset-3 col-md-4">
                        @if(Sentry::getUser()->hasAccess('admin'))
                        	<input class="btn-primary btn" type="submit" value="Update Memberships">
                        @endif
					</div> 

			</form>

		</fieldset>
	</div>
</div>

<script>
	$(document).ready(function(){
		$(".to-switch").bootstrapSwitch();
		$(".datepicker").datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			maxDate: '-18Y',
			minDate: '-100Y'
		});
		$('.mobile').mask("999-999-9999");

	});
</script>

@stop
