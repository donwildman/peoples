@extends('layouts.login')

{{-- Content --}}
@section('content')


	<div class="row">
		<div class="col-md-4 col-md-offset-4 text-center">
			
			<h2>Reset Password</h2>
		


			<form class="form" action="/reset-password" method="post" role="form">
				{{ Form::token() }}
				
				<div class="form-group {{ ($errors->has('email') ? 'has-error' : '') }}" >
					<label class="control-label sr-only" for="email">E-mail</label>
					
					<input name="email" id="email" value="{{ Input::old('email') }}" type="email" class="form-control input-sm" placeholder="E-mail Address">
					<span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
					
				</div>
				<button class="btn btn-primary" type="submit">Reset Password</button>
					

		  </form>
		

		</div>
	</div>


@stop