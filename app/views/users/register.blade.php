@extends('layouts.public')



{{-- Content --}}
@section('content')
<div class="col-md-12">


	<h2><i class="fa fa-user"></i> Register New User</h2>

		<div class="well">

			{{ Form::open(['url'=>'/register', 'class' => 'form-horizontal', 'role'=>'form']) }}

				<div class="form-group {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
					<label class="col-md-3 control-label" for="first_name">{{ trans('messages.first_name') }}:</label>
					<div class="col-md-4">
						<input name="first_name" id="first_name" value="{{ Input::old('first_name') }}" type="text" class="form-control" placeholder="First Name">
						<span class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</span>
					</div>
				</div>

				<div class="form-group {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
					<label class="col-md-3 control-label" for="last_name">{{ trans('messages.last_name') }}:</label>
					<div class="col-md-4">
						<input name="last_name" id="last_name" value="{{ Input::old('last_name') }}" type="text" class="form-control" placeholder="Last Name">
						<span class="help-block">{{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}</span>
					</div>
				</div>

				<div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
					<label class="col-md-3 control-label" for="email">{{ trans('messages.email') }}:</label>
					<div class="col-md-4">
						<input name="email" id="email" value="{{ Input::old('email') }}" type="text" class="form-control" placeholder="E-mail">
						<span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
					</div>
				</div>

				<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}" >
					<label class="col-md-3 control-label" for="password">{{ trans('messages.password') }}:</label>
					<div class="col-md-4">
						<input name="password" value="" type="password" class="form-control" placeholder="Password">
						<span class="help-block">{{ ($errors->has('password') ?  $errors->first('password') : '') }}</span>
					</div>
				</div>

				<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
					<label class="col-md-3 control-label" for="password_confirmation">{{ trans('messages.confirm_password') }}:</label>
					<div class="col-md-4">
						<input name="password_confirmation" value="" type="password" class="form-control" placeholder="Password Again">
						<span class="help-block">{{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}</span>
					</div>
				</div>

			<div class="form-group {{ ($errors->has('mobile')) ? 'has-error' : '' }}">
				<label class="col-md-3 control-label" for="mobile">{{ trans('messages.mobile') }}:</label>
				<div class="col-md-4">
					{{ Form::text('mobile', null, ['class'=>'form-control mobile', 'id'=>'mobile', 'placeholder'=>'Mobile Number']) }}
					<span class="help-block">{{ ($errors->has('mobile') ? $errors->first('mobile') : '') }}</span>
				</div>
			</div>

				<div class="form-group">
					<label class="col-md-3 control-label" for="language">{{ trans('messages.language') }}:</label>
					<div class="col-md-4">
						<select name="language" class="form-control">
							{{ LanguageManager::generateDropDown(LanguageManager::getLanguage()) }}
						</select>
					</div>
				</div>

				<div class="form-group {{ ($errors->has('birthday')) ? 'has-error' : '' }}">
					<label class="col-md-3 control-label" for="birthday">{{ trans('messages.birthday') }}:</label>
					<div class="col-md-4">
						<input name="birthday" id="birthday" value="{{ Input::old('birthday') }}" type="text" class="form-control datepicker" placeholder="Date of Birth">
						<span class="help-block">{{ ($errors->has('birthday') ? $errors->first('birthday') : '') }}</span>
					</div>
				</div>


				<div class="col-md-4 col-md-offset-2">
					<div class="form-group">
						<input class="btn-primary btn" type="submit" value="{{ trans('messages.register') }}">
						<input class="btn btn-default" type="reset" value="Reset">
					</div>
				</div>
			{{ Form::close() }}
			<div class="clearfix"></div>
		</div>
	



</div>

<script>
	$(document).ready(function(){

		$(".datepicker").datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			maxDate: '-18Y',
			minDate: '-100Y'
		});
		$('.mobile').mask("999-999-9999");

	});
</script>


@stop