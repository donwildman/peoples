@extends('layouts.default')


{{-- Content --}}
@section('content')
<div class="row">
	<div class="col-md-12">

		<h2><i class="fa fa-user"></i><span class="break"></span> Suspend {{ $myUser->email }}</h2>

		<div class="well">

			<form class="form-horizontal" action="/suspend/{{ $myUser->id }}" method="post">
				{{ Form::token() }}

				<div class="form-group {{ ($errors->has('suspendTime')) ? 'error' : '' }}" for="suspendTime">
					<label class="col-md-2 control-label" for="suspendTime">Duration:</label>
					<div class="col-md-5">
						<div class="input-group">
							<input name="suspendTime" id="suspendTime" value="{{ Request::old('suspendTime') }}" type="text" class="form-control" placeholder="Minutes" required>
							<span class="input-group-btn">
								<button class="btn btn-inverse" type="submit">Suspend User</button>
						  	</span>
						</div>


						{{ ($errors->has('suspendTime') ? $errors->first('suspendTime') : '') }}
					</div>
				</div>




		  </form>

			<div class="clearfix"></div>
		</div>

	</div>
</div>
@stop