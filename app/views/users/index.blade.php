@extends('layouts.default')

@section('content')
<div class="row">
<div class="col-md-12">

			<h2><i class="fa fa-user"></i> Current Users
				<a href="/users/add" class="btn btn-sm btn-primary pull-right" title="Add a new User"><i class="fa fa-plus"></i> Add User</a>
			</h2>



			<table class="table table-condensed table-striped table-hover">
				<thead>
					<th>User</th>
					<th width="150">Status</th>
					<th width="250">Options</th>
				</thead>
				<tbody>
					@foreach ($allUsers as $myUser)
						<tr>
							<td>
								<i class="famfamfam-flag-{{ $myUser->language }}"></i>&nbsp;
								<a href="/user/{{ $myUser->id }}">{{ $myUser->first_name }} {{ $myUser->last_name }}</a>

							</td>
							<td>{{ $userStatus[$myUser->id] }} </td>
							<td width="250">
								<a class="btn btn-sm btn-info" href="/edit/{{ $myUser->id}}"><i class="fa fa-edit"></i> Edit</a>
								@if($userStatus[$myUser->id] == 'Suspended')
									<a class="btn btn-sm btn-success" href="/unsuspend/{{ $myUser->id}}"><i class="fa fa-check-square-o"></i> Activate</a>
								@else
									<a class="btn btn-sm btn-warning" href="/suspend/{{ $myUser->id}}"><i class="fa fa-exclamation-triangle"></i> Suspend</a>
								@endif
								<a class="btn btn-sm btn-danger" href="/delete/{{ $myUser->id}}"><i class="fa fa-trash"></i> Delete</a></td>

						</tr>
					@endforeach
				</tbody>
			</table>



		</div>
	</div><!-- .box -->

</div>


@stop
