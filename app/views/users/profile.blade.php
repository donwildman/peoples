@extends('layouts.public')


{{-- Content --}}
@section('content')
<?php
	use Carbon\Carbon;
	$languages = Session::get('languages');
?>
<div class="row">
	<div class="col-md-12">

		<h2><i class="fa fa-user"></i><span class="break"></span> {{ trans('messages.profile') }}</h2>

		<div class="well">
			<div class="row">
				<div class="col-md-8 m-b-30">
					<p><strong>{{ trans('messages.first_name') }}:</strong> {{ $user->first_name }} </p>
					<p><strong>{{ trans('messages.last_name') }}:</strong> {{ $user->last_name }} </p>
					<p><strong>{{ trans('messages.email') }}:</strong> {{ $user->email }}</p>
					<p><strong>{{ trans('messages.language') }}:</strong> {{ $languages[$user->language] }}</p>
					<p><strong>{{ trans('messages.birthday') }}:</strong> {{ $user->birthday }}</p>
					<a href="/profile/edit" class="btn btn-info">Edit Profile</a>
				</div>
				<div class="col-md-4 text-right">
					<p><em>Account created: {{ Carbon::parse($user->created_at)->format('d jS F') }}</em></p>
					<p><em>Last Updated: {{ Carbon::parse($user->updated_at)->format('D jS F') }}</em></p>
				</div>
			</div>
		</div>

	</div>
</div>


@stop
