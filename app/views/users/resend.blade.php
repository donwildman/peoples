@extends('layouts.public')


{{-- Content --}}
@section('content')
<div class="row">
	<div class="col-md-12">

		<h2><i class="fa fa-user"></i><span class="break"></span> Resend Activation</h2>


		{{ Form::open(['url'=>'/resend', 'class'=>'form-horizontal', 'role'=>'form']) }}

			<div class="form-group {{ ($errors->has('email') ? 'error' : '') }}" >
				<label class="col-md-3 control-label" for="email">E-mail</label>
				<div class="col-md-5">
					{{ Form::email('email', Input::old('email')), ['class'=>'form-control', 'placeholder' => 'E-mail'] }}
					<span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
				</div>
			</div>

			<div class="col-md-5 col-md-offset-3">
				<button class="btn btn-primary" type="submit">Resend Activation</button>
			</div>

		{{ Form::close() }}



	</div><!-- .box -->

</div>

@stop