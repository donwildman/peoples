@extends('layouts.public')



{{-- Content --}}
@section('content')
<div class="row">
	<div class="col-md-12">

		<h2><i class="fa fa-edit"></i> Edit
			@if ($user->email == Sentry::getUser()->email)
			Your
			@else
			{{ $user->first_name }}'s
			@endif
			Profile</h2>

		<fieldset>
			<legend>Personal Details</legend>
			{{ Form::model($user, ['url' => '/profile/edit/', 'role'=>'form', 'class' => 'form-horizontal']) }}

			<div class="form-group {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
				<label class="col-md-3 control-label" for="first_name">{{ trans('messages.first_name') }}:</label>
				<div class="col-md-4">
					{{ Form::text('first_name', null, ['class'=>'form-control', 'id'=>'first_name', 'placeholder'=>'First Name']) }}
					<span class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</span>
				</div>
			</div>

			<div class="form-group {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
				<label class="col-md-3 control-label" for="last_name">{{ trans('messages.last_name') }}:</label>
				<div class="col-md-4">
					{{ Form::text('last_name', null, ['class'=>'form-control', 'id'=>'last_name', 'placeholder'=>'Last Name']) }}
					<span class="help-block">{{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}</span>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label" for="email">{{ trans('messages.email') }}:</label>
				<div class="col-md-4">
					<p class="form-control-static">{{ $user->email }}</p>
				</div>
			</div>

			<div class="form-group {{ ($errors->has('mobile')) ? 'has-error' : '' }}">
				<label class="col-md-3 control-label" for="mobile">{{ trans('messages.mobile') }}:</label>
				<div class="col-md-4">
					{{ Form::text('mobile', null, ['class'=>'form-control mobile', 'id'=>'mobile', 'placeholder'=>'Mobile Number']) }}
					<span class="help-block">{{ ($errors->has('mobile') ? $errors->first('mobile') : '') }}</span>
				</div>
				<div class="col-md-5">
					<p class="form-control-static text-muted">Format: 073-123-4567</p>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label" for="language">{{ trans('messages.language') }}:</label>
				<div class="col-md-4">
					<select name="language" class="form-control">
						{{ LanguageManager::generateDropDown($user->language) }}
					</select>
				</div>
			</div>


			<div class="form-group {{ ($errors->has('birthday')) ? 'has-error' : '' }}">
				<label class="col-md-3 control-label" for="birthday">{{ trans('messages.birthday') }}:</label>
				<div class="col-md-4">
					{{ Form::text('birthday', null, ['class'=>'form-control datepicker', 'id'=>'birthday', 'placeholder'=>'Date of Birth']) }}
					<span class="help-block">{{ ($errors->has('birthday') ? $errors->first('birthday') : '') }}</span>
				</div>
			</div>

			<div class="col-md-4 col-md-offset-3">
				<button class="btn-primary btn" type="submit" >{{ trans('messages.update') }}</button>
			</div>

			{{ Form::close() }}
		</fieldset>


	</div>
</div>


<div class="row m-t-30">
	<div class="col-md-12">


		<fieldset>
			<legend><i class="fa fa-edit"></i> Change Password</legend>

		{{ Form::open(['url' => '/profile/change-password', 'role'=>'form', 'class' => 'form-horizontal']) }}

		<div class="form-group {{ $errors->has('oldPassword') ? 'has error' : '' }}" for="oldPassword">
			<label class="col-md-3 control-label" for="oldPassword">{{ trans('messages.old_password') }}</label>
			<div class="col-md-4">
				<input name="oldPassword" value="" type="password" class="form-control" placeholder="Old Password">
				{{ ($errors->has('oldPassword') ? $errors->first('oldPassword') : '') }}
			</div>
		</div>

		<div class="form-group {{ $errors->has('newPassword') ? 'has error' : '' }}" for="newPassword">
			<label class="col-md-3 control-label" for="newPassword">{{ trans('messages.password') }}</label>
			<div class="col-md-4">
				<input name="newPassword" value="" type="password" class="form-control" placeholder="New Password">
				{{ ($errors->has('newPassword') ?  $errors->first('newPassword') : '') }}
			</div>
		</div>

		<div class="form-group {{ $errors->has('newPassword_confirmation') ? 'has error' : '' }}" for="newPassword_confirmation">
			<label class="col-md-3 control-label" for="newPassword_confirmation">{{ trans('messages.confirm_password') }}</label>
			<div class="col-md-4">
				<input name="newPassword_confirmation" value="" type="password" class="form-control" placeholder="New Password Again">
				{{ ($errors->has('newPassword_confirmation') ? $errors->first('newPassword_confirmation') : '') }}
			</div>
		</div>


		<div class="col-md-offset-3 col-md-4">
			<button class="btn-primary btn" type="submit" >{{ trans('messages.update') }}</button>
		</div>


		{{ Form::close() }}
		</fieldset>
	</div>
</div>



<script>
	$(document).ready(function(){

		$(".datepicker").datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			maxDate: '-18Y',
			minDate: '-100Y'
		});
		$('.mobile').mask("999-999-9999");

	});
</script>

@stop
