@extends('layouts.login')

@section('content')

		{{ Form::open(['url' => '/login', 'class'=>'form-signin']) }}
			<h2 class="form-signin-heading">{{ trans('messages.login') }}</h2>
			<label for="inputEmail" class="sr-only">{{ trans('messages.email') }}</label>
			<input type="email" name="email" id="inputEmail" class="form-control" placeholder="{{ trans('messages.email') }}" required autofocus>
			<label for="inputPassword" class="sr-only">{{ trans('messages.password') }}</label>
			<input type="password" name="password" id="inputPassword" class="form-control" placeholder="{{ trans('messages.password') }}" required>
			<div class="checkbox">
			  <label>
				<input type="checkbox" name="rememberMe" value="1"> Remember me
			  </label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">{{ trans('messages.submit') }}</button>
            <a class="btn btn-lg btn-info btn-block" href="/register">{{ trans('messages.register') }}</a>
            <div class="p-t-30 text-center"><a href="/reset-password">Forgot your password?</a></div>
		{{ Form::close() }}

    @stop