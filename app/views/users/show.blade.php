@extends('layouts.default')


{{-- Content --}}
@section('content')

	<?php
	$languages = Session::get('languages');
	?>

<div class="row m-t-30">
	<div class="col-md-12">

			<h2><i class="fa fa-user"></i><span class="break"></span> Account Profile</h2>


	
  	<div class="well clearfix">


		    	<p><strong>First Name:</strong> {{{ $myUser->first_name }}} </p>

		    	<p><strong>Last Name:</strong> {{{ $myUser->last_name }}} </p>

		    <p><strong>Email:</strong> {{{ $myUser->email }}}</p>

				<p><strong>Language:</strong> {{ $languages[$myUser->language] }}</p>
				<p><strong>Date of Birth:</strong> {{{ $myUser->birthday }}}</p>
		    
			<a href="/edit/{{ $myUser->id}}" class="btn btn-info">Edit Profile</a>


			<p><em>Account created: {{{ $myUser->created_at }}}</em></p>
			<p><em>Last Updated: {{{ $myUser->updated_at }}}</em></p>

	</div>

	<h4>Group Memberships:</h4>
	<div class="well">
	    <ul>
	    	@if (count($myGroups) >= 1)
		    	@foreach ($myGroups as $group)
					<li>{{{ $group['name'] }}}</li>
				@endforeach
			@else 
				<li>No Group Memberships.</li>
			@endif
	    </ul>
	</div>


	</div><!-- .box -->

</div>


@stop
