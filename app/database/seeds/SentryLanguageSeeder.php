<?php

class SentryLanguageSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('languages')->delete();
		
		Language::create(array('name' => 'Afrikaans', 'code' => 'af'));
		Language::create(array('name' => 'English', 'code' => 'en'));
		Language::create(array('name' => 'French', 'code' => 'fr'));
		Language::create(array('name' => 'German', 'code' => 'de'));
		Language::create(array('name' => 'Hebrew', 'code' => 'he'));
		Language::create(array('name' => 'isiZulu', 'code' => 'zu'));
		Language::create(array('name' => 'Italian', 'code' => 'it'));
		Language::create(array('name' => 'Russian', 'code' => 'ru'));
		Language::create(array('name' => 'Spanish', 'code' => 'es'));

	}

}