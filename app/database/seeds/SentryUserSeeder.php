<?php

class SentryUserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();

		Sentry::createUser(array(
	        'email'    => 'admin@admin.com',
	        'password' => 'sentryadmin',
			'first_name' => 'Test',
			'last_name' => 'Administrator',
			'mobile' => '+27831234567',
			'language' => 'en',
			'birthday' => '1970-05-18',
	        'activated' => 1,
	    ));

	    Sentry::createUser(array(
	        'email'    => 'user@user.com',
	        'password' => 'sentryuser',
			'first_name' => 'Test',
			'last_name' => 'User',
			'mobile' => '+27831234567',
			'language' => 'en',
			'birthday' => '1980-05-10',
	        'activated' => 1,
	    ));
	}

}