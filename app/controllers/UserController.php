<?php

class UserController extends BaseController {
	

	public function __construct()
	{
		$this->beforeFilter('csrf', array('on' => 'post'));

		$throttleProvider = Sentry::getThrottleProvider();

		$throttleProvider->enable();
	}

	public function getIndex()
	{
		$data['allUsers'] = Sentry::findAllUsers();

		$data['userStatus'] = array();
		foreach ($data['allUsers'] as $myUser) {
			if ($myUser->isActivated())
			{
				$data['userStatus'][$myUser->id] = "Active";
			} 
			else 
			{
				$data['userStatus'][$myUser->id] = "Not Active";
			}

			$throttle = Sentry::getThrottleProvider()->findByUserId($myUser->id);

			if($throttle->isSuspended())
			{
				$data['userStatus'][$myUser->id] = "Suspended";
			}

			if($throttle->isBanned())
			{
				$data['userStatus'][$myUser->id] = "Banned";
			}

		}
		
		return View::make('users.index',$data);
			
	}

	public function getShow($id)
	{
		$data['myUser'] = Sentry::findUserById($id);
		$data['myGroups'] = $data['myUser']->getGroups();
		return View::make('users.show',$data);
	}

	public function getProfile()
	{
		$data['user'] = Sentry::getUser();
		return View::make('users.profile',$data);
	}

	/**
	 * Register a new Admin user.
	 *
	 * @return Response
	 */
	public function getAdd()
	{
		// Show the register form
		return View::make('users.add');
	}

	public function postAdd()
	{
	
		// Gather Sanitized Input
		$input = Input::all();

		// Set Validation Rules
		$rules = array (
			'first_name' => 'required|alpha',
			'last_name' => 'required|alpha',
			'email' => 'required|min:4|max:32|email',
			'password' => 'required|min:6|alpha_dash|confirmed',
			'password_confirmation' => 'required',
			'mobile' => 'required',
			'birthday' => 'required|date',
			);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{
			// Validation has failed
			return Redirect::to(URL::previous())->withErrors($v)->withInput();
		}
		else 
		{

			try {
				//Attempt to register the user. 
				$user = Sentry::register([
					'first_name'=>$input['first_name'], 
					'last_name' => $input['last_name'],
					'email' => $input['email'], 
					'password' => $input['password'],
					'mobile' => $input['mobile'],
					'language' => $input['language'],
					'birthday' => $input['birthday'],
					]);

				$userGroup = Sentry::getGroupProvider()->findById(2);
				$user->addGroup($userGroup);

				//success!
				Session::flash('success', 'The Admin account has been created.');
				return Redirect::to('/');

			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
			    Session::flash('error', 'Email address required.');
			    return Redirect::to(URL::previous())->withErrors($v)->withInput();
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    Session::flash('error', 'User with that Email Address already exists.');
			    return Redirect::to(URL::previous())->withErrors($v)->withInput();
			}

		}
	}

	/**
	 * Register a new User user.
	 *
	 * @return Response
	 */
	public function getRegister()
	{
		// Show the register form
		return View::make('users.register');
	}

	public function postRegister()
	{

		// Gather Sanitized Input
		$input = Input::all();

		// Set Validation Rules
		$rules = array (
			'first_name' => 'required|alpha',
			'last_name' => 'required|alpha',
			'email' => 'required|min:4|max:32|email',
			'password' => 'required|min:6|alpha_dash|confirmed',
			'password_confirmation' => 'required',
			'mobile' => 'required',
			'birthday' => 'required|date',
			);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{
			// Validation has failed
			return Redirect::to(URL::previous())->withErrors($v)->withInput();
		}
		else
		{

			try {
				//Attempt to register the user.
				$user = Sentry::register([
					'first_name'=>$input['first_name'],
					'last_name' => $input['last_name'],
					'email' => $input['email'],
					'password' => $input['password'],
					'mobile' => $input['mobile'],
					'language' => $input['language'],
					'birthday' => $input['birthday'],
					]);

				//Get the activation code & prep data for email
				$data['activationCode'] = $user->GetActivationCode();
				$data['email'] = $input['email'];
				$data['userId'] = $user->getId();

				// this is only here to bypass the emails for this exercise, it would not be in production!
				Session::put('activation_code', $data['activationCode']);
				Session::put('userId', $data['userId']);

				//send email with link to activate.
				Mail::send('emails.auth.welcome', $data, function($m) use($data)
				{
					$m->to($data['email'])->subject('Welcome to Peoples People Manager');
				});

				//success!
				Session::flash('success', 'Your account has been created. Check your email for the activation link.');
				return Redirect::to('/home');

			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
			    Session::flash('error', 'Email address required.');
			    return Redirect::to(URL::previous())->withErrors($v)->withInput();
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    Session::flash('error', 'User with that Email Address already exists.');
			    return Redirect::to(URL::previous())->withErrors($v)->withInput();
			}

		}
	}

	public function getActivate($userId = null, $activationCode = null) {
		try 
		{
			// temporary
			Session::forget('activation_code');
			Session::forget('userId');
		    // Find the user
		    $user = Sentry::findUserById($userId);

		    // Attempt user activation
		    if ($user->attemptActivation($activationCode))
		    {
		        // User activation passed
		        
		    	//Add this person to the user group. 
		    	$userGroup = Sentry::getGroupProvider()->findById(1);
		    	$user->addGroup($userGroup);

				
				
		        Session::flash('success', 'Your account has been activated. Please log in.');
				return Redirect::to('/login');
		    }
		    else
		    {
		        // User activation failed
		        Session::flash('error', 'There was a problem activating this account. Please contact the system administrator.');
				return Redirect::to('/');
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('error', trans('auth.failed'));
			return Redirect::to('/');
		}
		catch (Cartalyst\SEntry\Users\UserAlreadyActivatedException $e)
		{
		    Session::flash('error', 'You have already activated this account.');
			return Redirect::to('/');
		}


	}

	public function getLogin()
	{
		// Show the login form
		return View::make('users.login_form');
	}

	public function postLogin() 
	{
		// Gather Sanitized Input
		$input = array(
			'email' => Input::get('email'),
			'password' => Input::get('password'),
			'rememberMe' => Input::get('rememberMe',0)
			);

		// Set Validation Rules
		$rules = array (
			'email' => 'required|min:4|max:32|email',
			'password' => 'required|min:6'
			);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{
			// Validation has failed
			return Redirect::to('/login')->withErrors($v)->withInput();
		}
		else 
		{
			try
			{
				//Check for suspension or banned status
				$throttle = Sentry::findThrottlerByUserLogin($input['email']);
			    $throttle->check();

			    // Set login credentials
			    $credentials = array(
			        'email'    => $input['email'],
			        'password' => $input['password']
			    );

			    // Try to authenticate the user
			    $user = Sentry::authenticate($credentials, ($input['rememberMe'] == 1 ? true : false));
				
				LanguageManager::setLanguage($user->language);

			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    Session::flash('error', trans('auth.failed') );
				return Redirect::to('/login')->withErrors($v)->withInput();
			}
			catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
			{
			    echo 'User not activated.';
			     Session::flash('error', trans('auth.activate'));
				return Redirect::to('/login')->withErrors($v)->withInput();
			}
			catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
			{
			    $time = $throttle->getSuspensionTime();
			    Session::flash('error', trans('auth.throttle'));
				return Redirect::to('/login')->withErrors($v)->withInput();
			}
			catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
			{
			    Session::flash('error', 'You have been banned.');
				return Redirect::to('/login')->withErrors($v)->withInput();
			}

			if ($user->hasAccess('admin'))
			{
				return Redirect::to('/users');
			}
			else
			{
				return Redirect::to('/profile');
			}
			
		}
	}
	
	public function getLogout() 
	{
		Sentry::logout();
		return Redirect::to('/login');
	}

	public function getResetpassword() {
		return View::make('users.reset');
	}

	public function postResetpassword () {
		$input = array(
			'email' => Input::get('email')
			);
		
		$rules = array (
			'email' => 'required|min:4|max:32|email'
			);
		
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{
			// Validation has failed
			return Redirect::to('/reset-password')->withErrors($v)->withInput();
		}
		else 
		{
			try
			{
			    $user      = Sentry::getUserProvider()->findByLogin($input['email']);
			    $data['resetCode'] = $user->getResetPasswordCode();
			    $data['userId'] = $user->getId();
			    $data['email'] = $input['email'];

			    // Email the reset code to the user
				Mail::send('emails.auth.reset', $data, function($m) use($data)
				{
				    $m->to($data['email'])->subject('Password Reset Confirmation');
				});

				Session::flash('info', 'If a user was found then an email has bees sent to the address provided');
			    return Redirect::to('/login');

			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				Session::flash('info', 'If a user was found then an email has bees sent to the address provided');
				return Redirect::to('/login');
			}
		}

	}

	public function getResend()
	{
		//Show the Resend Activation Form
		return View::make('users.resend');
	}

	public function postResend()
	{

		// Gather Sanitized Input
		$input = array(
			'email' => Input::get('email')
			);

		// Set Validation Rules
		$rules = array (
			'email' => 'required|min:4|max:32|email'
			);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{
			// Validation has failed
			return Redirect::to('/resend')->withErrors($v)->withInput();
		}
		else 
		{

			try {
				//Attempt to find the user. 
				$user = Sentry::getUserProvider()->findByLogin(Input::get('email'));


				if (!$user->isActivated())
				{
					//Get the activation code & prep data for email
					$data['activationCode'] = $user->GetActivationCode();
					$data['email'] = $input['email'];
					$data['userId'] = $user->getId();

					//send email with link to activate.
					Mail::send('emails.auth.welcome', $data, function($m) use ($data)
					{
					    $m->to($data['email'])->subject('Activate your account');
					});

					//success!
			    	Session::flash('success', 'Check your email for the confirmation link.');
			    	return Redirect::to('/');
				}
				else 
				{
					Session::flash('error', 'That account has already been activated.');
			    	return Redirect::to('/');
				}

			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
			    Session::flash('error', 'Login field required.');
			    return Redirect::to('/resend')->withErrors($v)->withInput();
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    Session::flash('error', 'User already exists.');
			    return Redirect::to('/resend')->withErrors($v)->withInput();
			}


		}


	}

	public function getReset($userId = null, $resetCode = null) {
		try
		{
		    // Find the user
		    $user = Sentry::findUserById($userId);
		    $newPassword = $this->_generatePassword(8,8);

		    // Attempt to reset the user password
		    if ($user->attemptResetPassword($resetCode, $newPassword))
		    {
		        // Password reset passed
		        // 
		        // Email the reset code to the user

			    //Prepare New Password body
			    $data['newPassword'] = $newPassword;
			    $data['email'] = $user->getLogin();

			    Mail::send('emails.auth.newpassword', $data, function($m) use($data)
				{
				    $m->to($data['email'])->subject('New Password Information | City Lodge Admin CMS');
				});

				Session::flash('success', 'Your password has been changed. Check your email for the new password.');
			    return Redirect::to('/');
		        
		    }
		    else
		    {
		        // Password reset failed
		    	Session::flash('error', 'There was a problem.  Please contact the system administrator.');
			    return Redirect::to('users/resetpassword');
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    echo 'User does not exist.';
		}
	}

	public function getClearreset($userId = null) {
		try
		{
		    // Find the user
		    $user = Sentry::findUserById($userId);

		    // Clear the password reset code
		    $user->clearResetPassword();

		    echo "clear.";
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    echo 'User does not exist';
		}
	}

	public function getProfileEdit()
	{

		$data['user'] = Sentry::getUser();
		return View::make('users.profile_edit',$data);


		
	}

	public function postProfileEdit() {
		// Gather Sanitized Input
		$input = Input::except(['_token']);

		// Set Validation Rules
		// Set Validation Rules
		$rules = array (
			'first_name' => 'required|alpha',
			'last_name' => 'required|alpha',
			'mobile' => 'required',
			'birthday' => 'required|date',
		);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{
			// Validation has failed
			return Redirect::to('/profile/edit/')->withErrors($v)->withInput();
		}
		else 
		{
			try
			{
				$user = Sentry::getUser();
				$user->first_name = $input['first_name'];
				$user->last_name = $input['last_name'];
				$user->mobile = $input['mobile'];
				$user->birthday = $input['birthday'];
				$user->language = $input['language'];

				// Update the user
				if ($user->save())
				{
					LanguageManager::setLanguage($user->language);
					
					// User information was updated
					Session::flash('success', 'Profile updated.');
					return Redirect::to('/profile');
				}
				else
				{
					// User information was not updated
					Session::flash('error', 'Profile could not be updated.');
					return Redirect::to('/profile/edit');
				}


			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    Session::flash('error', 'User already exists.');
				return Redirect::to('users/edit/' . $id);
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    Session::flash('error', 'User was not found.');
				return Redirect::to('users/edit/' . $id);
			}
		}
	}

	public function postChangeProfilePassword()
	{
		// Gather Sanitized Input
		$input = array(
			'oldPassword' => Input::get('oldPassword'),
			'newPassword' => Input::get('newPassword'),
			'newPassword_confirmation' => Input::get('newPassword_confirmation')
		);

		// Set Validation Rules
		$rules = array (
			'oldPassword' => 'required|min:6',
			'newPassword' => 'required|min:6|confirmed',
			'newPassword_confirmation' => 'required'
		);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{
			return Redirect::to('/profile/edit/')->withErrors($v)->withInput();
		}
		else
		{
			try
			{

					$user = Sentry::getUser();

					if ($user->checkPassword($input['oldPassword']))
					{
						$resetCode = $user->getResetPasswordCode();
						if ($user->attemptResetPassword($resetCode, $input['newPassword']))
						{
							Session::flash('success', 'Your password has been changed.');
							return Redirect::to('/profile');
						}
						else
						{
							Session::flash('error', 'Your password could not be changed.');
							return Redirect::to('/profile/edit');
						}
					} else {
						// The oldPassword did not match the password in the database. Abort.
						Session::flash('error', 'You did not provide the correct password.');
						return Redirect::to('/profile/edit');
					}

			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
				Session::flash('error', 'Login field required.');
				return Redirect::to('/profile/edit');
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
				Session::flash('error', 'User already exists.');
				return Redirect::to('/profile/edit');
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				Session::flash('error', 'User was not found.');
				return Redirect::to('/profile/edit');
			}
		}
	}

	public function getEdit($id)
	{

		$data['user'] = Sentry::findUserById($id);
		$data['userGroups'] = $data['user']->getGroups();
		$data['allGroups'] = Sentry::findAllGroups();
		return View::make('users.edit',$data);


		
	}

	public function postEdit($id) {
		// Gather Sanitized Input
		$input = Input::except(['_token']);


		// Set Validation Rules
		$rules = array (
			'first_name' => 'required',
			'last_name' => 'required',
			'mobile' => 'required',
			'birthday' => 'required|date',
			);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{
			// Validation has failed
			return Redirect::to('/edit/' . $id)->withErrors($v)->withInput();
		}
		else 
		{
			try
			{
				$user = Sentry::findUserById($id);
				$user->first_name = $input['first_name'];
				$user->last_name = $input['last_name'];
				$user->mobile = $input['mobile'];
				$user->birthday = $input['birthday'];
				$user->language = $input['language'];

				// Update the user
				if ($user->save())
				{
					// User information was updated
					Session::flash('success', 'Profile updated.');
					return Redirect::to('/user/'. $id);
				}
				else
				{
					// User information was not updated
					Session::flash('error', 'Profile could not be updated.');
					return Redirect::to('/edit/' . $id);
				}


			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    Session::flash('error', 'User already exists.');
				return Redirect::to('users/edit/' . $id);
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    Session::flash('error', 'User was not found.');
				return Redirect::to('users/edit/' . $id);
			}
		}
	}

	public function postChangePassword($id)
	{
		// Gather Sanitized Input
		$input = array(
			'oldPassword' => Input::get('oldPassword'),
			'newPassword' => Input::get('newPassword'),
			'newPassword_confirmation' => Input::get('newPassword_confirmation')
			);

		// Set Validation Rules
		$rules = array (
			'oldPassword' => 'required|min:6',
			'newPassword' => 'required|min:6|confirmed',
			'newPassword_confirmation' => 'required'
			);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{
			return Redirect::to('users/edit/' . $id)->withErrors($v)->withInput();
		}
		else 
		{
			try
			{
			    
				//Get the current user's id.
				Sentry::check();
				$currentUser = Sentry::getUser();

			   	//Do they have admin access?
				if ( $currentUser->hasAccess('admin')  || $currentUser->getId() == $id)
				{
					// Either they are an admin, or they are changing their own password. 
					$user = Sentry::findUserById($id);	
					if ($user->checkHash($input['oldPassword'], $user->getPassword())) 
			    	{
				    	//The oldPassword matches the current password in the DB. Proceed.
				    	$user->password = $input['newPassword'];

				    	if ($user->save())
					    {
					        // User saved
					        Session::flash('success', 'Your password has been changed.');
							return Redirect::to('user/'. $id);
					    }
					    else
					    {
					        // User not saved
					        Session::flash('error', 'Your password could not be changed.');
							return Redirect::to('users/edit/' . $id);
					    }
					} else {
						// The oldPassword did not match the password in the database. Abort. 
						Session::flash('error', 'You did not provide the correct password.');
						return Redirect::to('users/edit/' . $id);
					}
				} else {
					Session::flash('error', 'You don\'t have access to that user.');
					return Redirect::to('/');
				}			   			    
			}
			catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
			{
			    Session::flash('error', 'Login field required.');
				return Redirect::to('users/edit/' . $id);
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    Session::flash('error', 'User already exists.');
				return Redirect::to('users/edit/' . $id);
			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
			    Session::flash('error', 'User was not found.');
				return Redirect::to('users/edit/' . $id);
			}
		}
	}

	public function postUpdatememberships($id)
	{
		try 
		{
			//Get the current user's id.

		   	//Do they have admin access?
			if ( Sentry::getUser()->hasAccess('admin'))
			{
				$user = Sentry::findUserById($id);
				$allGroups = Sentry::findAllGroups();
				$permissions = Input::get('permissions');
				
				$statusMessage = '';
				
				
				foreach ($allGroups as $group) {
					
					if (isset($permissions[$group->id])) 
					{
						//The user should be added to this group
						if ($user->addGroup($group))
					    {
					        $statusMessage .= "Added to " . $group->name . "<br />";
					    }
					    else
					    {
					        $statusMessage .= "Could not be added to " . $group->name . "<br />";
					    }
					} else {
						// The user should be removed from this group
						if ($user->removeGroup($group))
					    {
					        $statusMessage .= "Removed from " . $group->name . "<br />";
					    }
					    else
					    {
					        $statusMessage .= "Could not be removed from " . $group->name . "<br />";
					    }
					}

				}
				
				Session::flash('info', $statusMessage);
				return Redirect::to('user/'. $id);
			} 
			else 
			{
				Session::flash('error', 'You don\'t have access to that user.');
				return Redirect::to('/');
			}
	
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('error', trans('auth.failed'));
			return Redirect::to('users/edit/' . $id);
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
		    Session::flash('error', 'Trying to access unidentified Groups.');
			return Redirect::to('users/edit/' . $id);
		}
	}

	public function getSuspend($id)
	{
		try
		{
		    //Get the current user's id.
			$currentUser = Sentry::getUser();

		   	//Do they have admin access?
			if ( $currentUser->hasAccess('admin'))
			{
				$data['myUser'] = Sentry::findUserById($id);
				return View::make('users.suspend')->with($data);
			} else {
				Session::flash('error', 'You are not allowed to do that.');
				return Redirect::to('/');
			}

		}
		catch (Cartalyst\Sentry\UserNotFoundException $e)
		{
		    Session::flash('error', 'There was a problem accessing that user\s account.');
			return Redirect::to('/users');
		}
	}

	public function postSuspend($id)
	{
		// Gather Sanitized Input
		$input = array(
			'suspendTime' => Input::get('suspendTime')
			);

		// Set Validation Rules
		$rules = array (
			'suspendTime' => 'required|numeric'
			);

		//Run input validation
		$v = Validator::make($input, $rules);

		if ($v->fails())
		{
			// Validation has failed
			return Redirect::to('users/suspend/' . $id)->withErrors($v)->withInput();
		}
		else 
		{
			try
			{
				//Prep for suspension
				$throttle = Sentry::getThrottleProvider()->findByUserId($id);

				//Set suspension time
				$throttle->setSuspensionTime($input['suspendTime']);

				// Suspend the user
    			$throttle->suspend();

    			//Done.  Return to users page.
    			Session::flash('success', "User has been suspended for " . $input['suspendTime'] . " minutes.");
				return Redirect::to('users');

			}
			catch (Cartalyst\Sentry\UserNotFoundException $e)
			{
			    Session::flash('error', 'There was a problem accessing that user\'s account.');
				return Redirect::to('/users');
			}
		}
	}

	public function removeSuspend($id)
	{

			try
			{
				//Prep for suspension
				$throttle = Sentry::getThrottleProvider()->findByUserId($id);

				// Suspend the user
				$throttle->unsuspend();

				//Done.  Return to users page.
				Session::flash('success', "User has been un-suspended");
				return Redirect::to('users');

			}
			catch (Cartalyst\Sentry\UserNotFoundException $e)
			{
				Session::flash('error', 'There was a problem accessing that user\'s account.');
				return Redirect::to('/users');
			}

	}

	public function getDelete($id)
	{
		try
		{
		    // Find the user using the user id
		    $user = Sentry::findUserById($id);
			if($user->id == Sentry::getUser()->id)
			{
				Session::flash('error','You cannot delete your own account!');
				return Redirect::to('/users');
			}
		    // Delete the user
		    if ($user->delete())
		    {
				
				
		        // User was successfully deleted
		        Session::flash('success', 'That user has been deleted.');
				return Redirect::to('/users');
		    }
		    else
		    {
		        // There was a problem deleting the user
		        Session::flash('error', 'There was a problem deleting that user.');
				return Redirect::to('/users');
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    Session::flash('error', 'There was a problem accessing that user\s account.');
			return Redirect::to('/users');
		}
	}
	
	private function _generatePassword($length=9, $strength=4) {
		$vowels = 'aeiouy';
		$consonants = 'bcdfghjklmnpqrstvwxz';
		if ($strength & 1) {
			$consonants .= 'BCDFGHJKLMNPQRSTVWXZ';
		}
		if ($strength & 2) {
			$vowels .= "AEIOUY";
		}
		if ($strength & 4) {
			$consonants .= '23456789';
		}
		if ($strength & 8) {
			$consonants .= '@#$%';
		}
	 
		$password = '';
		$alt = time() % 2;
		for ($i = 0; $i < $length; $i++) {
			if ($alt == 1) {
				$password .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
			} else {
				$password .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
			}
		}
		return $password;
	}

}