<?php
Route::get('/change-language/{code}', function($code){

	LanguageManager::setLanguage($code);

	return Redirect::to(URL::previous());
});

	Route::get('/home', 'HomeController@getWelcome');

	Route::get('/logout', 'UserController@getLogout');
	Route::get('/profile', 'UserController@getProfile');
	Route::post('/profile', 'UserController@postProfile');

	Route::get('activate/{id}/{code}', 'UserController@getActivate');
	Route::get('reset/{id}/{code}', 'UserController@getReset');
	Route::get('reset-password', 'UserController@getResetpassword');
	Route::post('reset-password', 'UserController@postResetpassword');
	Route::get('resend', 'UserController@getResend');
	Route::post('resend', 'UserController@postResend');
	
	Route::group(array('before' => 'auth'), function()
	{
		
		Route::get('/users/add', 'UserController@getAdd');
		Route::post('/users/add', 'UserController@postAdd');
		
		Route::get('/users', 'UserController@getIndex');
		Route::get('/unsuspend/{id}', 'UserController@removeSuspend');
		Route::get('/suspend/{id}', 'UserController@getSuspend');
		Route::post('/suspend/{id}', 'UserController@postSuspend');

		Route::get('/user/{id}', 'UserController@getShow');
		Route::get('/edit/{id}', 'UserController@getEdit');
		Route::post('/edit/{id}', 'UserController@postEdit');
		
		Route::get('/delete/{id}', 'UserController@getDelete');

		Route::get('/profile/edit', 'UserController@getProfileEdit');
		Route::post('/profile/edit', 'UserController@postProfileEdit');
		Route::post('/profile/change-password', 'UserController@postChangeProfilePassword');

		
	});

		Route::get('/', function()
		{
			if ( !Sentry::check())
			{
				return Redirect::to('/login');
			}
			else
			{
				$user = Sentry::getUser();
				if($user->hasAccess('admin'))
				{
					return Redirect::to('/users');
				} else {
					return View::make('home');
				}

			}
		});

		Route::get('/login', 'UserController@getLogin');
		Route::post('/login', 'UserController@PostLogin');

		Route::get('/register', 'UserController@getRegister');
		Route::post('/register', 'UserController@PostRegister');



