<?php

namespace Peoples\LanguageManager;


class LanguageManager
{

    protected $languages;


    public function __construct()
    {
       $this->languages = \Language::orderBy('name')->lists('name','code');
    }

    public function generateDropDown($selected='en')
    {
        \Log::info('selected lang = '. $selected);
        $options = '';
        foreach($this->languages as $key=>$value)
        {
            $sel = ($key == $selected) ? ' selected="selected"' : '';
            $options .= '<option value="'.$key.'" '.$sel.'><i class="famfamfam-flag-'.$key.'"></i>&nbsp;'.$value.'</option>';
        }

        return $options;
    }
    public function generateDropList()
    {
        $options = '<ul class="dropdown-menu">';
        foreach($this->languages as $key=>$value)
        {
            $options .= '<li><a href="/change-language/'.$key.'"><i class="famfamfam-flag-'.$key.'"></i>&nbsp;'.$value.'</a></li>';
        }
        $options .= '</ul>';
        return $options;
    }

    public function setLanguage($code='en')
    {
        if(!empty($code))
        {
            \App::setLocale($code);
            \Config::set('app.locale', $code);

            \Log::info(\Config::get('app.locale'));
            
            \Session::put('current_language', $code);
            return true;
        }
        return false;
    }

    public function getLanguage()
    {
        return \Session::get('current_language');
    }

    public function getHumanLanguage()
    {
        return $this->languages[\Session::get('current_language', 'en')];
    }

}