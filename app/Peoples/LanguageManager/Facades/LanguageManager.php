<?php

namespace Peoples\LanguageManager\Facades;

use Illuminate\Support\Facades\Facade;

class LanguageManager extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'LanguageManager';
    }

}
