<?php
namespace Peoples;

use Illuminate\Support\ServiceProvider;

class PeoplesServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind('LanguageManager', 'Peoples\LanguageManager\LanguageManager');
    }
}