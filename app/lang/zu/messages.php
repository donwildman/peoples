<?php

return array(

	'changeLabel' => 'Guqula Ulimi: ',

	'submit'     	=> 'Beka Phansi',
	'update'     	=> 'Guqula',
	'view'     		=> 'bheka',
	'cancel'     	=> 'Khawuka',

	'first_name' => 'Igama',
	'last_name' => 'isibongo',
	'email' => 'Imeyili',
	'mobile' => 'Umakhalekhukhwini',
	'language' => 'Ulimi',
	'birthday' => 'Usuku Lokuzalwa',
	'password' => 'Iphasiwedi',
	'old_password' => 'Iphasiwedi Endala',
	'confirm_password' => 'Uqinisekise Iphasiwedi',
	'login' => 'Ngena ngemvume',
	'logout' => 'Phuma',
	'profile' => 'Phrofayele',
	'register' => 'Ukubhalisa',
);