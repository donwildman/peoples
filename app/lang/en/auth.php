<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'User not found.',
    'throttle' => 'Your account has been suspended for :seconds seconds.',
    'activate' => 'You have not yet activated this account. <a href="/de/users/resend">Resend activation?</a>',

];
