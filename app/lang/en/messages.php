<?php

return array(

	'changeLabel' => 'Change Language: ',

	'submit'     => 'Submit',
	'update'     => 'Update',
	'view'     => 'View',
	'cancel'     => 'Cancel',
	'change' => 'Update',
	'first_name' => 'First Name',
	'last_name' => 'Last Name',
	'email' => 'Email',
	'mobile' => 'Mobile',
	'language' => 'Language',
	'birthday' => 'Date of Birth',
	'password' => 'Password',
	'old_password' => 'Old Password',
	'confirm_password' => 'Confirm Password',
	'login' => 'Log In',
	'logout' => 'Log Out',
	'profile' => 'Profile',
	'register' => 'Register',

);