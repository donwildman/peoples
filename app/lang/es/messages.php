<?php

return array(

	'changeLabel' => 'Cambiar Idioma: ',

	'submit'     	=> 'Entregar',
	'update'     	=> 'Actualizar',
	'view'     		=> 'Mirar',
	'cancel'     	=> 'Cancelar',

	'first_name' => 'Nombre de Pila',
	'last_name' => 'Apellido',
	'email' => 'Correo electrónico',
	'mobile' => 'Teléfono móvil',
	'language' => 'Idioma',
	'birthday' => 'Cumpleaños',
	'password' => 'Contraseña',
	'old_password' => 'Contraseña Anterior',
	'confirm_password' => 'Confirmar Contraseña',
	'login' => 'Iniciar Sesión',
	'logout' => 'Cerrar Sesión',
	'profile' => 'Perfil',
	'register' => 'Registro',
);