<?php

return array(

	'changeLabel' => 'שנה שפה: ',

	'submit'     	=> 'שלח',
	'update'     	=> 'עדכון',
	'view'     		=> 'נוף',
	'cancel'     	=> 'לְבַטֵל',

	'first_name' => 'שם פרטי',
	'last_name' => 'שם משפחה',
	'email' => 'אֶלֶקטרוֹנִי',
	'mobile' => 'Mobile',
	'language' => 'שפה',
	'birthday' => 'יוֹם הוּלֶדֶת',
	'password' => 'סיסמה',
	'old_password' => 'סיסמה ישנה',
	'confirm_password' => 'אשר סיסמא',
	'login' => 'כניסה',
	'logout' => 'להתנתק',
	'profile' => 'פּרוֹפִיל',
	'register' => 'הירשם',
);