<?php

return array(

	'changeLabel' => 'Cambia lingua: ',

	'submit'     	=> 'Invio',
	'update'     	=> 'Aggiornare',
	'view'     		=> 'La Monstra',
	'cancel'     	=> 'Annulla',

	'first_name' => 'Nome',
	'last_name' => 'Cognome',
	'email' => 'E-mail',
	'mobile' => 'Cellulare',
	'language' => 'Lingua',
	'birthday' => 'Compleanno',
	'password' => 'Parola d\'ordine',
	'old_password' => 'Vecchia Password',
	'confirm_password' => 'Conferma Password',
	'login' => 'Accesso',
	'logout' => 'Disconnettersi',
	'profile' => 'Profilo',
	'register' => 'Registro',
);