<?php

return array(

	'changeLabel' => 'Verander Taal: ',

	'submit'     	=> 'Indien',
	'update'     	=> 'Opdateer',
	'view'     		=> 'Beskou',
	'cancel'     	=> 'Kanselleer',

	'first_name' => 'Eerste Naam',
	'last_name' => 'Familie Naam',
	'email' => 'Epos',
	'mobile' => 'Loopfoon',
	'language' => 'Taal',
	'birthday' => 'Geboortsdatum',
	'password' => 'Wagwoord',
	'old_password' => 'Voorige Wagwoord',
	'confirm_password' => 'Bevestig Wagwoord',
	'login' => 'Teken Aan',
	'logout' => 'Teken Uit',
	'profile' => 'Profiel',
	'register' => 'Registreer',
);