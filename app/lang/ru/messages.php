<?php

return array(

	'changeLabel' => 'Изменить язык: ',

	'submit'     	=> 'Отправить',
	'update'     	=> 'Обновить',
	'view'     		=> 'Посмотреть',
	'cancel'     	=> 'Отмена',

	'first_name' => 'имя',
	'last_name' => 'фамилия',
	'email' => 'Эл. адрес',
	'mobile' => 'Сотовый телефон',
	'language' => 'язык',
	'birthday' => 'день рождения',
	'password' => 'пароль',
	'old_password' => 'Старый пароль',
	'confirm_password' => 'Подтвердите Пароль',
	'login' => 'авторизоваться',
	'logout' => 'выйти',
	'profile' => 'профиль',
	'register' => 'Pегистр',
);