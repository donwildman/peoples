<?php

return array(

	'changeLabel' => 'Sprache ändern: ',

	'submit'     	=> 'Einreichen',
	'update'     	=> 'Aktualisieren',
	'view'     		=> 'Sehen',
	'cancel'     	=> 'Abmelden',

	'first_name' => 'Vorname',
	'last_name' => 'Familienname',
	'email' => 'Email',
	'mobile' => 'Mobile',
	'language' => 'Sprache',
	'birthday' => 'Geburtstag',
	'password' => 'Passwort',
	'old_password' => 'Altes Passwort',
	'confirm_password' => 'Bestätigen Passwort',
	'login' => 'Anmeldung',
	'logout' => 'Ausloggen',
	'profile' => 'Profil',
	'register' => 'Registrieren'
);