<?php

return array(

	'changeLabel' => 'Changer de Langue: ',

	'submit'     	=> 'Soumettre',
	'update'     	=> 'Mettre à jour',
	'view'     		=> 'Vue',
	'cancel'     	=> 'Annuler',

	'first_name' => 'Prénom',
	'last_name' => 'Nom de famille',
	'email' => 'Email',
	'mobile' => 'Téléphone portable',
	'language' => 'La langue',
	'birthday' => 'Anniversaire',
	'password' => 'Mot de Passe',
	'old_password' => 'Ancien Mot de Passe',
	'confirm_password' => 'Confirmez le Mot de Passe',
	'login' => 'S\'identifier',
	'logout' => 'Se déconnecter',
	'profile' => 'Profil',
	'register' => 'Registre',
);